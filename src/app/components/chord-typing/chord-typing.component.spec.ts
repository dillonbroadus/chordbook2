import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChordTypingComponent } from './chord-typing.component';

describe('ChordTypingComponent', () => {
  let component: ChordTypingComponent;
  let fixture: ComponentFixture<ChordTypingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChordTypingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChordTypingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
