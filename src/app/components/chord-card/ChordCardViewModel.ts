export class ChordCardViewModel {
	constructor(
		public active: boolean,
		public rootName: string,
		public octave: string = "",
		public flavorDescription: string = ""
	) {}
}