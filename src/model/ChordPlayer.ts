import WebAudioFontPlayer, { Instrument, InstrumentInfo } from "webaudiofont";
import { IChordDefinition } from "./Chords";

export class ChordPlayer {
	private innerPlayer: WebAudioFontPlayer;
	private audioCtx: AudioContext;
	private currInstr?: InstrumentInfo;

	constructor(ctx?: AudioContext) {
		this.audioCtx = ctx || new AudioContext();
		this.innerPlayer = new WebAudioFontPlayer();
	}

	getAllInstruments(): string[] {
		return this.innerPlayer.loader.instrumentTitles();
	}

	loadInstrument(name: string) {
		var index = this.getAllInstruments().findIndex((val) => val === name);
		if (index === -1) {
			throw `Could not find ${name}`;
		}
		this.currInstr = this.innerPlayer.loader.instrumentInfo(index);
		this.innerPlayer.loader.startLoad(
			this.audioCtx,
			this.currInstr.url,
			this.currInstr.variable
		);
		this.innerPlayer.loader.decodeAfterLoading(
			this.audioCtx,
			this.currInstr.variable
		);
	}
	/*
    We only support a granularity of 1 beat at 120 BPM and we play the chord IMMEDIATELY
    120 beats per minute means 2 beats per second, thus 1 beat/.5 sec. Therefore
    the duration of a chord will be numberOfQuarterNotes * .5
    */
	playChordFor(chord: number[], numberOfQuarterNotes: number) {
		this.innerPlayer.queueChord(
			this.audioCtx,
			this.audioCtx.destination,
			this.getInstrumentData(),
			0,
			chord,
			numberOfQuarterNotes * 0.5
		);
	}

	scheduleChords(chords: { notes: number[]; beats: number }[]) {
		let currentTime = this.audioCtx.currentTime;
		let duration = 0;
		for (let chord of chords) {
			duration = chord.beats * 0.5;

			this.innerPlayer.queueChord(
				this.audioCtx,
				this.audioCtx.destination,
				this.getInstrumentData(),
				currentTime,
				chord.notes,
				duration
			);
			currentTime += duration;
		}
	}

	// TODO: Fix weird click sound from stopping chords
	stopAllSound() {
		this.innerPlayer.cancelQueue(this.audioCtx);
	}

	getInstrumentData() {
		if (!this.currInstr) {
			return null;
		}
		return (window as { [key: string]: any })[this.currInstr.variable];
	}
}
