import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChordCardComponent } from './chord-card.component';

describe('ChordCardComponent', () => {
  let component: ChordCardComponent;
  let fixture: ComponentFixture<ChordCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChordCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChordCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
