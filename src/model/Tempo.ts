export class Tempo {
	private static wholeNoteIn120 = 2;
	get beatDuration() {
		let beatDivisionIn120 = Tempo.wholeNoteIn120 / this.beatDivision;
		return (120 / this.bpm) * beatDivisionIn120;
	}
	constructor(
		public bpm: number,
		public beatsInMeasure: number,
		public beatDivision: number
	) {}
}
