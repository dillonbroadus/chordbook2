declare module 'halfmoon' {
  export function onDOMContentLoaded(): void;
  export function toggleSidebar(): void;
}
