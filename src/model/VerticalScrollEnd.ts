export enum VerticalScrollEnd {
	Top,
	Bottom,
	Neither
}