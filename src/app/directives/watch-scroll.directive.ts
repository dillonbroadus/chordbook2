import { AfterViewInit, Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { VerticalScrollEnd } from 'src/model/VerticalScrollEnd';
import ResizeObserver from 'resize-observer-polyfill';
import { fromEvent, Subscription } from 'rxjs';

@Directive({
  selector: '[appWatchScroll]'
})
export class WatchScrollDirective implements OnDestroy, AfterViewInit, OnInit {
  @Output()
  verticalScrollAtEnd = new EventEmitter<{ end: VerticalScrollEnd, scrollableArea: number }>();

  private nativeElement: HTMLElement;
  private resizeObserver?: ResizeObserver;
  private scrollSubscription?: Subscription;

  constructor(element: ElementRef) { 
    this.nativeElement = element.nativeElement as HTMLElement;
  }

  ngOnInit(): void {
    this.scrollSubscription = fromEvent(this.nativeElement, 'scroll')
      .subscribe(() => this.checkVerticalScroll());
    this.resizeObserver = new ResizeObserver(() => this.checkVerticalScroll());
  }

  ngAfterViewInit(): void {
    this.resizeObserver!.observe(this.nativeElement);
  }

  private checkVerticalScroll() {
    let scrollableArea = this.nativeElement.scrollHeight - this.nativeElement.clientHeight;

    if(this.nativeElement.scrollTop === scrollableArea) {
      this.verticalScrollAtEnd.emit({ end: VerticalScrollEnd.Bottom, scrollableArea: scrollableArea });
    } else if (this.nativeElement.scrollTop === 0) {
      this.verticalScrollAtEnd.emit({ end: VerticalScrollEnd.Top, scrollableArea: scrollableArea });
    } else {
      this.verticalScrollAtEnd.emit({ end: VerticalScrollEnd.Neither, scrollableArea: scrollableArea });
    }
  }

  ngOnDestroy(): void {
    this.scrollSubscription!.unsubscribe();
    this.resizeObserver!.disconnect();
  }
}
