export interface SongSummary {
  chordString?: string;
  createdOn: Date;
  name: string;
  id: number;
}
