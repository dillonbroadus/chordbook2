import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SongSummaryEntry } from 'src/model/SongSummaryEntry';

@Component({
  selector: 'app-song-summary-card',
  templateUrl: './song-summary-card.component.html',
  styleUrls: ['./song-summary-card.component.css'],
})
export class SongSummaryCardComponent implements OnInit {
  @Input()
  songEntry?: SongSummaryEntry;

  @Output()
  deleteRequested = new EventEmitter();

  @Output()
  songSelected = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
