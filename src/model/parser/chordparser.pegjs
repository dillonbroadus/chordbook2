ChordProgression =
  Chord*

Chord = 
  root:RootNote _ acc:Accidental? _ oct:Octave? _ flavor:ChordFlavor? _ mods:Modifier* _ sustains:Sustain* {    
    return {
    	  root: root + (acc || ""),
        octave: oct || 4,
        beats: 1 + sustains.length,
        flavor: flavor || "major",
        modifiers: mods
    };
  }

Modifier =
  "b5" / "#5" / "6" / 
  "7" / "11" / "13" / 
  "9" / "no3"

ChordFlavor =
  MajorFlavor / MinorFlavor / DiminishedFlavor / DominantFlavor / AugmentedFlavor

AugmentedFlavor =
  ("Aug" / "aug" / "+") {
    return "augmented";
  }

DominantFlavor =
  ("Dom" / "dom") {
    return "dominant";
  }

DiminishedFlavor =
  ("Dim" / "dim" / "-") {
    return "diminished";
  }

MinorFlavor =
  ("Min" / "min" / "m") {
    return "minor";
  }

MajorFlavor =
 ("Maj" / "maj" / "M") {
   return "major";
 }

Accidental =
  accidental:("#" / "b") {
    return accidental;
  }

RootNote =
  [CDEFGAB]

Octave =
  [0-9]+ {
  	return parseInt(text());
  }

Sustain =
  _ sust:"/" _ {
    return sust;
  }
  
_ "whitespace"
  = [ \t\n\r]*

              