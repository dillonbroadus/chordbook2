declare module "webaudiofont" {
	export interface Instrument {
		zones: Array<Zone>;
	}

	export interface InstrumentInfo {
		url: string;
		variable: string;
		title: string;
	}

	export interface Zone {
		ahdsr: boolean;
		anchor: number;
		buffer: AudioBuffer;
		coarseTune: number;
		delay: number;
		file: string;
		fineTune: number;
		keyRangeHigh: number;
		keyRangeLow: number;
		loopEnd: number;
		loopStart: number;
		midi: number;
		originalPitch: number;
		sampleRate: number;
		sustain: number;
	}

	export interface WebAudioFontLoader {
		instrumentTitles(): string[];
		instrumentInfo(index: number): InstrumentInfo;
		startLoad(ctx: AudioContext, url: string, variableName: string): void;
		decodeAfterLoading(ctx: AudioContext, variableName: string): void;
	}

	class WebAudioFontPlayer {
		constructor();
		loader: WebAudioFontLoader;
		queueChord(
			ctx: AudioContext,
			destNode: AudioDestinationNode,
			instr: Instrument,
			atTime: number,
			notes: number[],
			duration: number
		): void;
		cancelQueue(ctx: AudioContext): void;
	}

	export default WebAudioFontPlayer;
}
