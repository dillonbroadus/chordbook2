import { Injectable } from '@angular/core';
import { SongSummary } from 'src/model/SongSummary';

@Injectable({
  providedIn: 'root',
})
export class SongsService {
  private songs: SongSummary[] = [
    { id: 1, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 2, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 3, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 4, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 5, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 6, createdOn: new Date('01/01/2021'), name: 'My Song' },
    { id: 7, createdOn: new Date('01/01/2021'), name: 'My Song' },
  ];

  private nextId = this.songs.length + 1;

  constructor() {}

  removeSong(song: SongSummary) {
    let index = this.songs.findIndex(s => s.id === song.id);

    if(index === -1) {
      throw new Error('Could not find the given song');
    }

    this.songs = this.songs.slice(0, index).concat(this.songs.slice(index + 1));
  }

  getSongs() {
    return this.songs;
  }

  addSong(song: SongSummary) {
    song.id = this.nextId;
    this.songs.push(song);
    this.nextId++;
  }
}
