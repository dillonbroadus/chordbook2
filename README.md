# Chordbook

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Rebuilding the parser

Simply run `npm run buildparser` in order to invoke the pegjs tool

## Live testing

Currently the app is not very functional in terms of adding new songs and editing them. However, the chord parser does indeed work.

To see it in action, run `ng serve` and navigate to `http://localhost:4200/`. Once there, simply hit the toggle switch to the right of the "Chordbook" link.
A prompt should appear asking you to input some chords and press enter. Try a string like `C///Dmin7///Emin7///Fmaj/Gdom/` and have fun with it!
