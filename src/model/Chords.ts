import { IParsedChord } from "./parser/chordparser";

export interface IChord {
	name: string;
	notes: number[];
}

export interface IChordDefinition {
	name: string;
	intervals: number[];
}

const noteMap: { [key: string]: number } = {
	C: 0,
	"C#": 1,
	Db: 1,
	D: 2,
	"D#": 3,
	Eb: 3,
	E: 4,
	F: 5,
	"F#": 6,
	Gb: 6,
	G: 7,
	"G#": 8,
	Ab: 8,
	A: 9,
	"A#": 10,
	Bb: 10,
	B: 11
};

// Worth noting that some of these chords should have some
// notes excluded due to the chord flavor's sound being
// "lost" in all the notes
export const ChordFlavors: {[key: string]: { name: string, intervals: number[] }} = {
	major: { name: "Major", intervals: [4, 3] },
	power: { name: "Power", intervals: [7] },
	dominant7: { name: "Dominant7", intervals: [4, 3, 3] },
	diminished: { name: "Diminished", intervals: [4, 2] },
	diminished7: { name: "Diminished7", intervals: [4, 2, 5] },
	major6: { name: "Major6", intervals: [4, 3, 2] },
	minor: { name: "Minor", intervals: [3, 4] },
	minor7: { name: "Minor7", intervals: [3, 4, 3] },
	major7: { name: "Major7", intervals: [4, 3, 4] },
	"major6/9": { name: "Major6/9", intervals: [4, 3, 2, 5] },
	major7b5: { name: "Major7b5", intervals: [4, 2, 5] },
	"major7#5": { name: "Major7#5", intervals: [4, 4, 3] },
	major9: { name: "Major9", intervals: [4, 3, 4, 3] },
	major11: { name: "Major11", intervals: [4, 3, 4, 3, 3] },
	major13: { name: "Major13", intervals: [4, 3, 4, 3, 3, 4] },
	minor6: { name: "Minor6", intervals: [3, 4, 2] },
	"minor6/9": { name: "Minor6/9", intervals: [3, 4, 2, 5] },
	major7b3: { name: "Major7b3", intervals: [3, 4, 4] }
};

export const ChordUtils = {
	calculateNoteNumber: function(noteName: string) {
		let matches = noteName.match(/([CDEFGAB][b#]?)([0-9]*)/);

		if(matches) {
			let baseNoteNumber = noteMap[matches[1]];

			let octaveNumber = matches[2] ? parseInt(matches[2]) : 4;
	
			let noteNumber = (octaveNumber + 1) * 12 + baseNoteNumber;
	
			return noteNumber;
		}

		throw new Error(`Could not parse ${noteName} into a proper note number`)
	},

	buildChordFromIntervals: function(root: number, intervals: number[]) {
		let notes = [root];

		intervals.reduce((sum, curr) => {
			let aggInterval = sum + curr;
			notes.push(aggInterval);
			return aggInterval;
		}, root);

		return notes;
	},

	// TODO: Add error checking for string-based chord root
	buildChord: function(
		rootName: string,
		definition: IChordDefinition
	): IChord {
		return {
			name: `${rootName} ${definition.name}`,
			notes: ChordUtils.buildChordFromIntervals(
				ChordUtils.calculateNoteNumber(rootName),
				definition.intervals
			)
		};
	},

	// TODO: Ridiculously inefficient
	getNoteNames: function(chord: IChord) {
		let noteNames = [];

		for (let note of chord.notes) {
			let octave = Math.floor(note / 12) - 1;

			let baseNoteValue = note - (octave + 1) * 12;

			let noteName = Object.keys(noteMap).find(
				k => noteMap[k] === baseNoteValue
			);

			noteNames.push(noteName);
		}

		return noteNames;
	},

	chordFromParsed: function(parsedInput: IParsedChord) {
		let chord = parsedInput.flavor + parsedInput.modifiers.join();
		let rootName = parsedInput.root + parsedInput.octave.toString();
		return {
			chord: ChordUtils.buildChord(rootName, ChordFlavors[chord]),
			beats: parsedInput.beats
		};
	}
};
