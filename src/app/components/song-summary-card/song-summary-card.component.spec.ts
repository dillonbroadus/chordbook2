import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SongSummaryCardComponent } from './song-summary-card.component';

describe('SongSummaryCardComponent', () => {
  let component: SongSummaryCardComponent;
  let fixture: ComponentFixture<SongSummaryCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SongSummaryCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SongSummaryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
