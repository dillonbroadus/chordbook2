import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';
import * as halfmoon from 'halfmoon';
import { SongSummaryEntry } from 'src/model/SongSummaryEntry';
import { VerticalScrollEnd } from 'src/model/VerticalScrollEnd';
import { SongsService } from '../services/songs/songs.service';

@Component({
  selector: 'app-root',
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  @ViewChild('sidebar')
  private sideBar?: ElementRef;
  shouldShowShadowBox: boolean = false;
  isAuthenticated: boolean = false;
  selectedSong?: SongSummaryEntry;

  constructor(
    private songsService: SongsService,
    private changeDetector: ChangeDetectorRef
  ) {
  }

  songEntries: SongSummaryEntry[] = [];
  requestedToDelete?: SongSummaryEntry;

  print(data: any) {
    console.log(data);
  }

  toggleAuthenticated() {
    this.isAuthenticated = !this.isAuthenticated;

    halfmoon.toggleSidebar();

    if (this.isAuthenticated) {
      this.refreshSongList();
      this.selectedSong = this.songEntries.length === 0 ? undefined : this.songEntries[this.songEntries.length - 1];
    } else {
      this.songEntries = [];
    }
  }

  ngAfterViewInit(): void {
    halfmoon.onDOMContentLoaded();

    if (!this.isAuthenticated) {
      halfmoon.toggleSidebar();
    }

    // changing :root var's from the global and local stylesheets
    // doesn't seem to do anything, however this fixes the issue for now
    if (this.sideBar) {
      (this.sideBar.nativeElement as HTMLElement).style.backgroundColor =
        '#25282c';
    }
  }

  songSelected(songEntry: SongSummaryEntry) {
    this.songEntries[songEntry.index].isSelected = false;
    this.selectedSong = songEntry;
    this.songEntries[this.selectedSong.index].isSelected = false;
    this.changeDetector.detectChanges();
  }

  checkShadowBox(update: { end: VerticalScrollEnd; scrollableArea: number }) {
    if (update.end === VerticalScrollEnd.Bottom) {
      this.shouldShowShadowBox = false;
    } else if (update.scrollableArea > 0) {
      this.shouldShowShadowBox = true;
    }

    this.changeDetector.detectChanges();
  }

  confirmDelete() {
    // Attempt to delete from db
    this.songsService.removeSong(this.requestedToDelete!.song);
    this.requestedToDelete = undefined;
    this.refreshSongList();
  }

  requestDelete(songEntry: SongSummaryEntry) {
    this.requestedToDelete = songEntry;
    this.triggerRequestDeleteModal();    
  }

  private triggerRequestDeleteModal() {
    // BADPRACTICE: Change this. Router and Location API's do not trigger :target selector for modal styles.
    // Determine why href is different
    // TODO: What happens if this link is navigated to without a song selected?
    window.location.href = window.location.origin + '/#confirm-delete';
  }

  refreshSongList() {
    this.songEntries = this.songsService.getSongs()
      .map((song, idx) => { return { index: idx, isSelected: false, song: song } });
  }

  addSong() {
    this.songsService.addSong({
      id: 0,
      createdOn: new Date(),
      name: 'My Song',
    });
    this.refreshSongList();
  }
}

