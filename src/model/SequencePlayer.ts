export class SequencePlayer {
	audioCtx: AudioContext;
	mainGainNode: GainNode;
	hasBeenInitialized: boolean = false;
	stopped = false;
	private shouldLoop = false;
	private playHeadPosition = 0;
	private playing = false;

	smallestTimeDifference: number = 0.5;

	constructor(
		private sequence: Array<{
			sample: AudioBuffer;
			duration: number;
			noteCallBack?: () => void;
		}>
	) {
		this.audioCtx = new AudioContext();
	}

	playSample(sample: AudioBuffer, when: number, duration?: number) {
		this.checkAndInitialize();
		let sourceNode = this.audioCtx.createBufferSource();
		sourceNode.buffer = sample;
		sourceNode.connect(this.mainGainNode);
		sourceNode.onended = () => {
			sourceNode.disconnect();
		};
		sourceNode.start(when, 0, duration);
	}

	checkAndInitialize() {
		if (this.audioCtx.state !== "running") {
			this.audioCtx.resume();
		}
		if (!this.hasBeenInitialized) {
			this.mainGainNode = this.audioCtx.createGain();
			this.mainGainNode.connect(this.audioCtx.destination);
			this.hasBeenInitialized = true;
		}
	}

	playSequence(shouldLoop: boolean = false) {
		if (!this.playing) {
			this.checkAndInitialize();
			this.shouldLoop = shouldLoop;
			this.stopped = false;
			let time = this.audioCtx.currentTime;
			let nextTime = time;
			this.playing = true;
			let mainLoop = () => {
				if (!this.stopped) {
					if (nextTime <= this.audioCtx.currentTime) {
						if (this.playHeadPosition === this.sequence.length) {
							this.playHeadPosition = 0;
						}

						this.playSample(
							this.sequence[this.playHeadPosition].sample,
							0,
							this.sequence[this.playHeadPosition].duration
						);

						if (this.sequence[this.playHeadPosition].noteCallBack) {
							this.sequence[this.playHeadPosition].noteCallBack();
						}

						let timeDifference =
							this.audioCtx.currentTime - nextTime;

						nextTime +=
							this.smallestTimeDifference + timeDifference;

						console.log(`Current: ${this.audioCtx.currentTime}, Next: ${nextTime}`)

						this.playHeadPosition++;

						if (
							this.playHeadPosition === this.sequence.length &&
							this.shouldLoop === false
						) {
							this.playHeadPosition = 0;
							this.stopped = true;
							this.playing = false;
						}
					}
					requestAnimationFrame(mainLoop);
				}
			};
			mainLoop();
		}
	}

	pause() {
		this.stopAllNotes();
		this.stopped = true;
		this.playing = false;
		this.mainGainNode.disconnect();
		this.hasBeenInitialized = false;
		this.checkAndInitialize();
	}

	stopAndReset() {
		this.stopAllNotes();
		this.stopped = true;
		this.playing = false;
		this.shouldLoop = false;
		this.playHeadPosition = 0;
	}

	stopAllNotes() {
		this.mainGainNode.disconnect();
		this.hasBeenInitialized = false;
		this.checkAndInitialize();
	}
}
