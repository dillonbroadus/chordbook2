export interface IParsedChord {
	root: string;
	beats: number;
	modifiers: string[];
	flavor: string;
	octave: number;
}

export function parse(input: string): IParsedChord[];

export interface ILocation {
	offset: number;
	line: number;
	column: number;
}

export interface ISyntaxError {
	expected: any[];
	found: string;
	location: { start: ILocation; end: ILocation };
	message: string;
	name: string;
}
