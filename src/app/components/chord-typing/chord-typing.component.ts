import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import * as ChordParser from "src/model/parser/chordparser";
import { ChordPlayer } from "src/model/ChordPlayer";
import { ChordUtils } from "src/model/Chords";
import { ChordCardViewModel } from "../chord-card/ChordCardViewModel";
import { SongSummary } from "src/model/SongSummary";

@Component({
	selector: "app-chord-typing",
	templateUrl: "./chord-typing.component.html",
	styleUrls: ["./chord-typing.component.css"],
})
export class ChordTypingComponent {
	@Input()
	song?: SongSummary;

	@Output()
	songChange = new EventEmitter<SongSummary>();

	editingName: boolean = false;
	errorMessage?: string;
	hasError: boolean = false;
	input: string = "";
	chords?: ChordParser.IParsedChord[];
	player: ChordPlayer;

	chordCardViewModels = [] as ChordCardViewModel[];

	constructor() {
		this.player = new ChordPlayer();
		let instr = this.player.getAllInstruments()[0]; // piano
		this.player.loadInstrument(instr);
	}

	beginEditingName() {
		this.editingName = true;
	}

	applyNameChange(newName: string) {
		this.song!.name = newName;
		this.editingName = false;
	}

	parseInput(input: string) {
		try {
			this.chords = ChordParser.parse(input);
			this.chordCardViewModels = [];
			this.chords.forEach((c) => {
				this.chordCardViewModels.push(
					new ChordCardViewModel(
						false,
						c.root,
						c.octave.toString(),
						c.flavor + c.modifiers.join()
					)
				);

				for (let i = 0; i < c.beats - 1; i++) {
					this.chordCardViewModels.push(
						new ChordCardViewModel(false, "/")
					);
				}
			});

			this.hasError = false;
		} catch (error) {
			let syntaxError = error as ChordParser.ISyntaxError;
			this.hasError = true;
			this.errorMessage = syntaxError.message;
			this.clearChords();
		}
	}

	keyPressed(keyName: string) {
		if(keyName === 'Enter') {
			this.play();
		}
	}

	play() {
		this.player.stopAllSound();

		if(!this.chords) {
			return;
		}

		this.player.scheduleChords(
			this.chords.map((c) => {
				return {
					notes: ChordUtils.chordFromParsed(c).chord.notes,
					beats: c.beats,
				};
			})
		);
	}

	clearChords() {
		this.chords = [];
		this.chordCardViewModels = [];
	}

	contentChanged() {
		this.player.stopAllSound();
		if(this.input) {
			this.input = this.input.trim();

			if (this.input.length != 0) {
				this.parseInput(this.input);
			} else {
				this.clearChords();
				this.hasError = false;
			}
		} else {
			this.clearChords();
			this.hasError = false;
		}
	}
}
