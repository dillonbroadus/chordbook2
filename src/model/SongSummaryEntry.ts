import { SongSummary } from "./SongSummary";

export interface SongSummaryEntry {
	index: number;
	isSelected: boolean;
	song: SongSummary;
  }