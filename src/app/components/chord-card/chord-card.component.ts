import { Component, OnInit, Input } from "@angular/core";
import { ChordCardViewModel } from "./ChordCardViewModel";

@Component({
	selector: "app-chord-card",
	templateUrl: "./chord-card.component.html",
	styleUrls: ["./chord-card.component.css"]
})
export class ChordCardComponent implements OnInit {
	@Input()
	model?: ChordCardViewModel;

	constructor() {}

	ngOnInit() {}
}
