import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { SongSummaryCardComponent } from './components/song-summary-card/song-summary-card.component';
import { ChordCardComponent } from './components/chord-card/chord-card.component';
import { ChordTypingComponent } from './components/chord-typing/chord-typing.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule, NgModel } from '@angular/forms';
import { WatchScrollDirective } from './directives/watch-scroll.directive';

@NgModule({
  declarations: [AppComponent, SongSummaryCardComponent, ChordCardComponent, ChordTypingComponent, WatchScrollDirective],
  imports: [BrowserModule, AppRoutingModule, CommonModule, QuillModule.forRoot(), FormsModule],
  providers: [NgModel],
  bootstrap: [AppComponent],
})
export class AppModule {}
